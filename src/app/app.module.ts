import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListUserComponent } from './user/list-user/list-user.component';
import { LoginComponent } from './login/login.component';
import { AddUserComponent } from './user/add-user/add-user.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import {routing} from "./app.routing";
import {ReactiveFormsModule} from "@angular/forms";
import {ApiService} from "./service/api.service";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {TokenInterceptor} from "./core/interceptor";
import { ListOrderComponent } from './order/list-order/list-order.component';
import {LoginService} from "./service/login.service";
import {OrderService} from "./service/order.service";
import {UserService} from "./service/user.service";
import { EditOrderComponent } from './order/edit-order/edit-order.component';
import {DriverService} from "./service/driver.service";
import {TruckService} from "./service/truck.service";
import { ListDriverComponent } from './driver/list-driver/list-driver.component';
import { EditDriverComponent } from './driver/edit-driver/edit-driver.component';
import { AddDriverComponent } from './driver/add-driver/add-driver.component';
import { AddTruckComponent } from './truck/add-truck/add-truck.component';
import { ListTruckComponent } from './truck/list-truck/list-truck.component';
import { EditTruckComponent } from './truck/edit-truck/edit-truck.component';


@NgModule({
  declarations: [
    AppComponent,
    ListUserComponent,
    LoginComponent,
    AddUserComponent,
    EditUserComponent,
    ListOrderComponent,
    EditOrderComponent,
    ListDriverComponent,
    EditDriverComponent,
    AddDriverComponent,
    AddTruckComponent,
    ListTruckComponent,
    EditTruckComponent
  ],
  imports: [
    BrowserModule,
    routing,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    ApiService,
    LoginService,
    OrderService,
    UserService,
    DriverService,
    TruckService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi : true}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
