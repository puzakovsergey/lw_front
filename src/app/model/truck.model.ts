import {Good} from "./good.model";

export class Truck {

  id: number;
  licencePlate: any;
  orderId: any;
  currentCity: any;
  fixed: boolean;
  capacity: number;
  notes: any;
}
