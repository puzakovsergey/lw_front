import {User} from "./user.model";

export class ApiResponse {

  status: number;
  message: number;
  result: any;
  token: any;
  role: String;
  users: User[];
  body: any;
}
