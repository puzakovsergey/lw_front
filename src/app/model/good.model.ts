export class Good {

  id: number;
  cityForLoading: any;
  cityForUnloading: any;
  name: any;
  weight: number;
  orderID: number;
  dateReady: Date;
  dateLoaded: Date;
  dateUnload: Date;
  notes: any;

}
