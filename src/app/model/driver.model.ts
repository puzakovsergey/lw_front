import {User} from "./user.model";

export class Driver {

  id: number;
  user: User;
  orderId: number;
  firstName: any;
  lastName: any;
  birthDate: Date;
  currentCity: any;
  status: any;
}
