import {Good} from "./good.model";
import {Truck} from "./truck.model";
import {Driver} from "./driver.model";

export class Order {

  id: number;
  truck: Truck;
  driver: Driver;
  timeForDelivery: number;
  maxWeight: number;
  startDate: Date;
  endDate: Date;
  goods: Good[];

}
