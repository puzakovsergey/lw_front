import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {LoginService} from "../service/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  invalidLogin: boolean = false;
  constructor(private formBuilder: FormBuilder, private router: Router, private loginService: LoginService) { }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    const loginPayload = {
      username: this.loginForm.controls.username.value,
      password: this.loginForm.controls.password.value
    }
    this.loginService.login(loginPayload).subscribe(data => {
      debugger;
      if(data.status === 200) {
        window.localStorage.setItem('token', data.body.token);
        if(data.body.role.localeCompare('USER') === 0 ) {
          this.router.navigate(['list-order']);
        }
        if(data.body.role.localeCompare('ADMIN') === 0) {
          this.router.navigate(['list-user']);
        }
        if(data.body.role.localeCompare('DRIVER') === 0) {
          this.router.navigate(['list-driver']);
        }
        window.localStorage.removeItem("editUserRole");
        window.localStorage.setItem("editUserRole", data.body.role.toString());
      }else {
        this.invalidLogin = true;
        alert(data.statusText);
      }
    });
  }

  ngOnInit() {
    window.localStorage.removeItem('token');
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.required]
    });
  }



}
