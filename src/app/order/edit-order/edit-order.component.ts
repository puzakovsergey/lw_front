import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {Order} from "../../model/order.model";
import {ApiService} from "../../service/api.service";
import {OrderService} from "../../service/order.service";
import {Truck} from "../../model/truck.model";
import {Driver} from "../../model/driver.model";
import {Good} from "../../model/good.model";

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css']
})
export class EditOrderComponent implements OnInit {

  order: Order;
  editForm: FormGroup;
  goods: Good[];
  constructor(private formBuilder: FormBuilder,private router: Router, private orderService: OrderService) { }

  ngOnInit() {
    let orderId = window.localStorage.getItem("editOrderId");
    if(!orderId) {
      alert("Invalid action.")
      this.router.navigate(['list-order']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [''],
      truck: [],
      driver: [],
      timeForDelivery: [],
      maxWeight: [],
      startDate: [],
      endDate: [],
      goods: []
    });
    this.orderService.getOrderById(+orderId)
      .subscribe( data => {
        this.editForm.setValue({
          id: data.id,
          truck: data.truck.licencePlate,
          driver: data.driver.firstName + ' ' + data.driver.lastName,
          timeForDelivery: data.timeForDelivery,
          maxWeight: data.maxWeight,
          startDate: data.startDate,
          endDate: data.endDate,
          goods: data.goods,
        });
      })
  }

  onSubmit() {
  }

  deleteGood(order: Order) {
  }

  addGood() {
  }

}
