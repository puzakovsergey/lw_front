import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {Order} from "../../model/order.model";
import {ApiService} from "../../service/api.service";
import {OrderService} from "../../service/order.service";
import {ApiResponse} from "../../model/api.response";
import {delay} from "rxjs/operators";

@Component({
  selector: 'app-list-order',
  templateUrl: './list-order.component.html',
  styleUrls: ['./list-order.component.css']
})
export class ListOrderComponent implements OnInit {

  orders: Order[];
  order: Order;
  responce: ApiResponse;

  constructor(private router: Router, private apiService: ApiService, private orderService: OrderService) { }

  ngOnInit() {
    if(!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
    this.orderService.getOrders()
      .subscribe( data => {
        this.orders = data;
      });
  }

  deleteOrder(order: Order): void {
    this.orderService.deleteOrder(order.id)
      .subscribe( data => {
        if (data.status == 200) {
          alert('Order ID= ' + order.id + ' deleted');
          this.ngOnInit();
        } else {
          alert('Error on delete order ID= ' + order.id + ' ' + data.statusText)
        }
      })
  };

  editOrder(order: Order): void {
    window.localStorage.removeItem("editOrderId");
    window.localStorage.setItem("editOrderId", order.id.toString());
    this.router.navigate(['edit-order']);
  };

  addOrder(): void {
    if(!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
    this.orderService.createOrder()
      .subscribe(data => {
        if (data.status == 200) {
          alert('Order ID= ' + data.body.id + ' created');
          this.ngOnInit();
        } else {
          alert('Error on creation order ' + data.statusText );
        }
      })

  }

  refresh(): void {
    this.ngOnInit();
  };
}
