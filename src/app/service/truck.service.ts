import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Order} from "../model/order.model";
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api.response";
import {Truck} from "../model/truck.model";
import {map} from "rxjs/operators";
import {User} from "../model/user.model";

@Injectable()
export class TruckService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:8086/api/trucks';

  getTrucks() : Observable<Truck[]> {
    return this.http.get<Truck[]>(this.baseUrl + '/all');
  }

  getTruckById(id: number): Observable<Truck> {
    return this.http.get<Truck>(this.baseUrl + '/details?id=' + id);
  }

  createTruck(truck: Truck): Observable<HttpResponse<Truck>> {
    return  this.http.post<Truck>(this.baseUrl + '/new', truck, { observe:'response'});
  }


  deleteTruck(id: number): Observable<HttpResponse<String>> {
    return this.http.delete(this.baseUrl + '/delete?id=' + id, { observe:'response', responseType: 'text'});
  }

  updateTruck(truck: Truck): Observable<HttpResponse<Truck>> {
    return this.http.put<Truck>(this.baseUrl + "/update", truck, { observe:'response'});
  }

  getTruckByLicencePlate(licencePlate: String): Observable<Truck[]> {
    return this.http.get<Truck[]>(this.baseUrl + "/find_by_licenceplate?licencePlate=" + licencePlate);
  }

  getTrucksForOrder(order: Order): Observable<Truck[]> {
    return this.http.get<Truck[]>(this.baseUrl + "/get_trucks_for_order");
  }
}
