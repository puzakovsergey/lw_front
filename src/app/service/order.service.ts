import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Order} from "../model/order.model";
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api.response";
import {User} from "../model/user.model";

@Injectable()
export class OrderService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:8086/api/orders';

  getOrders() : Observable<Order[]> {
    return this.http.get<Order[]>(this.baseUrl + '/all');
  }

  getOrderById(id: number): Observable<Order> {
    return this.http.get<Order>(this.baseUrl + '/details?id=' + id);
  }

  createOrder(): Observable<HttpResponse<Order>> {
    return  this.http.get<Order>(this.baseUrl + '/new',{ observe:'response'});
  }

  deleteOrder(id: number): Observable<HttpResponse<string>> {
    return this.http.delete(this.baseUrl + '/delete?id=' + id, { observe:'response', responseType: 'text'});
  }

  setDriver(order: Order): Observable<HttpResponse<Order>> {
    return this.http.put<Order>(this.baseUrl + "/set_driver", order, { observe:'response'});
  }

  setTruck(order: Order): Observable<HttpResponse<Order>> {
    return this.http.put<Order>(this.baseUrl + "/set_truck", order, { observe:'response'});
  }
}
