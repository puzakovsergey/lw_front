import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Order} from "../model/order.model";
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api.response";
import {Driver} from "../model/driver.model";
import {Truck} from "../model/truck.model";
import {map} from "rxjs/operators";
import {User} from "../model/user.model";

@Injectable()
export class DriverService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:8086/api/drivers';

  getDrivers() : Observable<Driver[]> {
    return this.http.get<Driver[]>(this.baseUrl + '/all');
  }

  getDriverById(id: number): Observable<Driver> {
    return this.http.get<Driver>(this.baseUrl + '/details?id=' + id);
  }

  createDriver(driver: Driver): Observable<HttpResponse<Driver>> {
    return  this.http.post<Driver>(this.baseUrl + '/new', driver, { observe:'response'});
  }


  deleteDriver(id: number): Observable<HttpResponse<string>> {
    return this.http.delete(this.baseUrl + '/delete?id=' +  id, { observe:'response', responseType: 'text'});
  }

  updateDriver(driver: Driver): Observable<HttpResponse<Driver>> {
    return this.http.put<Driver>(this.baseUrl + "/update", driver, { observe:'response'});
  }

  getFreeDrivers(): Observable<HttpResponse<Driver[]>> {
    return this.http.get<Driver[]>(this.baseUrl + "/free_drivers", { observe:'response'});
  }

  getDriversForOrder(order:Order): Observable<HttpResponse<Driver[]>> {
    return this.http.put<Driver[]>(this.baseUrl + "/get_drivers_for_order", order, { observe:'response'});
  }

  getDriversByName(name: String): Observable<HttpResponse<Driver[]>> {
    return this.http.get<Driver[]>(this.baseUrl + "/find_by_name?name=" + name, { observe:'response'});
  }
}
