import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api.response";

@Injectable()
export class LoginService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:8086/api/authenticate';

  login(loginPayload) : Observable<HttpResponse<ApiResponse>> {
    return this.http.get<ApiResponse>(this.baseUrl+ '?username=' + loginPayload.username + '&password=' + loginPayload.password, { observe:'response'});
  }
}
