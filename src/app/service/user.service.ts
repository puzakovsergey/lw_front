import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {User} from "../model/user.model";
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api.response";
import {map} from "rxjs/operators";

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:8086/api/user';

  getUsers() : Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + '/all');
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(this.baseUrl + '/details?id=' + id);
  }

  createUser(user: User): Observable<HttpResponse<User>> {
    return this.http.post<User>(this.baseUrl + '/new', user, { observe:'response'});
  }

  updateUser(user: User): Observable<HttpResponse<User>> {
    return this.http.put<User>(this.baseUrl + '/update', user, { observe:'response'});
  }

  deleteUser(id: number): Observable<HttpResponse<string>> {
    return this.http.delete(this.baseUrl + '/delete?id=' + id, { observe:'response', responseType: 'text'});
  }
}
