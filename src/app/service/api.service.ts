import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {User} from "../model/user.model";
import {Observable} from "rxjs/index";
import {ApiResponse} from "../model/api.response";

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) { }
  baseUrl: string = 'http://localhost:8086/api/';

  login(loginPayload) : Observable<ApiResponse> {
    return this.http.get<ApiResponse>('http://localhost:8086/api/authenticate?username=' + loginPayload.username + '&password=' + loginPayload.password);
    //return this.http.get<ApiResponse>('http://localhost:8086/api/user?username=' + loginPayload.username + '&password=' + loginPayload.password);
  }

  getUsers() : Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl + 'user/all');
  }

  getUserById(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + id);
  }

  createUser(user: User): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl, user);
  }

  updateUser(user: User): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(this.baseUrl + user.id, user);
  }

  deleteUser(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(this.baseUrl + id);
  }
}
