import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular8-demo';

  constructor(private router: Router) { }

  orders() {
      if(!window.localStorage.getItem('token')) {
        this.router.navigate(['login']);
        return;
      }
      this.router.navigate(['list-order']);
      return;
  }

  users() {
    if(!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
    this.router.navigate(['list-user']);
    return;
  }

  drivers() {
    if(!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
    this.router.navigate(['list-driver']);
    return;
  }

  trucks() {
    if(!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
    this.router.navigate(['list-truck']);
    return;
  }
}
