import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {Driver} from "../../model/driver.model";
import {DriverService} from "../../service/driver.service";
import {User} from "../../model/user.model";

@Component({
  selector: 'app-edit-driver',
  templateUrl: './edit-driver.component.html',
  styleUrls: ['./edit-driver.component.css']
})
export class EditDriverComponent implements OnInit {

  driver: Driver;
  editForm: FormGroup;
  user: User;
  constructor(private formBuilder: FormBuilder,private router: Router, private driverService: DriverService) { }

  ngOnInit() {
    let driverId = window.localStorage.getItem("editDriverId");
    if(!driverId) {
      alert("Invalid action.")
      this.router.navigate(['list-driver']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      userLogin:[],
      birthDate: ['', Validators.required],
      currentCity: ['', Validators.required],
      status: ['', Validators.required],
      orderId: [],
    });
    this.driverService.getDriverById(+driverId)
      .subscribe( data => {
        this.editForm.setValue({
          id: data.id,
          firstName: data.firstName,
          lastName: data.lastName,
          userLogin: data.user.login,
          birthDate: data.birthDate,
          currentCity: data.currentCity,
          status: data.status,
          orderId: data.orderId,
        } );
      });
  }

  onSubmit() {
    this.driverService.updateDriver(this.editForm.value)
      .subscribe(
        data => {
          if(data.status === 200) {
            alert('Driver updated successfully.');
            this.router.navigate(['list-driver']);
          }else {
            alert(data.statusText);
          }
        },
        error => {
          alert(error);
        });
  }

}
