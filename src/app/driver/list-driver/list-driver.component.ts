import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {Order} from "../../model/order.model";
import {ApiService} from "../../service/api.service";
import {OrderService} from "../../service/order.service";
import {ApiResponse} from "../../model/api.response";
import {delay} from "rxjs/operators";
import {Driver} from "../../model/driver.model";
import {DriverService} from "../../service/driver.service";

@Component({
  selector: 'app-list-driver',
  templateUrl: './list-driver.component.html',
  styleUrls: ['./list-driver.component.css']
})
export class ListDriverComponent implements OnInit {

  drivers: Driver[];
  driver: Driver;
  responce: ApiResponse;

  constructor(private router: Router, private apiService: ApiService, private driverService: DriverService) { }

  ngOnInit() {
    if(!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
    this.driverService.getDrivers()
      .subscribe( data => {
        this.drivers = data;
      });
  }

  deleteDriver(driver: Driver): void {
    this.driverService.deleteDriver(driver.id)
      .subscribe( data => {
        if (data.status == 200) {
          alert('Driver deleted')
        } else {
          alert('Error')
        }
      })
    delay(10000);
    this.ngOnInit();
  };

  editDriver(driver: Driver): void {
    window.localStorage.removeItem("editDriverId");
    window.localStorage.setItem("editDriverId", driver.id.toString());
    this.router.navigate(['edit-driver']);
  };

  addDriver(): void {
    this.router.navigate(['add-driver']);
  }

  refresh(): void {
    this.ngOnInit();
  };
}
