import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {DriverService} from "../../service/driver.service";
import {User} from "../../model/user.model";

@Component({
  selector: 'app-add-driver',
  templateUrl: './add-driver.component.html',
  styleUrls: ['./add-driver.component.css']
})
export class AddDriverComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router, private driverService: DriverService) { }

  addForm: FormGroup;

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id: [],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      birthDate: ['', Validators.required],
      currentCity: ['', Validators.required]
    });
  }

  onSubmit() {
    this.driverService.createDriver(this.addForm.value)
      .subscribe( data => {
        if (data.status == 200) {
          alert('Driver ' + data.body.firstName + ' ' + data.body.lastName + ' created');
          this.router.navigate(['list-driver']);
        } else {
          alert(data.statusText)
        }
      });
  }

}
