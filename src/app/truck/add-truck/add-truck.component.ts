import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TruckService} from "../../service/truck.service";
import {Truck} from "../../model/truck.model";

@Component({
  selector: 'app-add-truck',
  templateUrl: './add-truck.component.html',
  styleUrls: ['./add-truck.component.css']
})
export class AddTruckComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router, private truckService: TruckService) { }

  addForm: FormGroup;
  truck: Truck;

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      id: [],
      licencePlate: ['', Validators.required],
      currentCity: ['', Validators.required],
      fixed: [true, Validators.required],
      capacity: ['', Validators.required],
      notes: ['', Validators.required],
    });

  }

  onSubmit() {
    this.truck = this.addForm.value;
    if (this.truck.licencePlate !== '') {
      this.truckService.createTruck(this.truck)
        .subscribe( data => {
          if (data.status == 200) {
            alert('Truck ' + data.body.licencePlate + ' created.')
            this.router.navigate(['list-truck']);
          } else {
            alert('Error on creation Truck ' + data.statusText);
          }
        });
    }
  }

}
