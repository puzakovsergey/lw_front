import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {Truck} from "../../model/truck.model";
import {TruckService} from "../../service/truck.service";

@Component({
  selector: 'app-edit-truck',
  templateUrl: './edit-truck.component.html',
  styleUrls: ['./edit-truck.component.css']
})
export class EditTruckComponent implements OnInit {

  truck: Truck;
  editForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private truckService: TruckService) { }

  ngOnInit() {
    let truckId = window.localStorage.getItem("editTruckId");
    if(!truckId) {
      alert("Invalid action.")
      this.router.navigate(['list-truck']);
      return;
    }
    this.editForm = this.formBuilder.group({
      id: [],
      licencePlate: ['', Validators.required],
      currentCity: ['', Validators.required],
      fixed: ['', Validators.required],
      capacity: ['', Validators.required],
      orderId: ['', Validators.required],
      notes: ['', Validators.required],
    });
    this.truckService.getTruckById(+truckId)
      .subscribe( data => {
        this.editForm.setValue(data);
      });
  }

  onSubmit() {
    this.truckService.updateTruck(this.editForm.value)
      .subscribe(
        data => {
          if(data.status === 200) {
            alert('Truck updated successfully.');
            this.router.navigate(['list-truck']);
          }else {
            alert(data.statusText);
          }
        },
        error => {
          alert(error);
        });
  }

}
