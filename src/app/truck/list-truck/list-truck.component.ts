import { Component, OnInit , Inject} from '@angular/core';
import {Router} from "@angular/router";
import {Order} from "../../model/order.model";
import {ApiService} from "../../service/api.service";
import {OrderService} from "../../service/order.service";
import {ApiResponse} from "../../model/api.response";
import {delay} from "rxjs/operators";
import {Truck} from "../../model/truck.model";
import {TruckService} from "../../service/truck.service";

@Component({
  selector: 'app-list-truck',
  templateUrl: './list-truck.component.html',
  styleUrls: ['./list-truck.component.css']
})
export class ListTruckComponent implements OnInit {

  trucks: Truck[];
  truck: Truck;
  responce: ApiResponse;

  constructor(private router: Router, private apiService: ApiService, private truckService: TruckService) { }

  ngOnInit() {
    if(!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    }
    this.truckService.getTrucks()
      .subscribe( data => {
        this.trucks = data;
      });
  }

  deleteTruck(truck: Truck): void {
    this.truckService.deleteTruck(truck.id)
      .subscribe( data => {
        if (data.status == 200) {
          alert('Truck ' + truck.licencePlate + ' deleted')
        } else {
          alert('Can not delete truck ' + truck.licencePlate );
        }
        this.ngOnInit();
      })
  };

  editTruck(truck: Truck): void {
    window.localStorage.removeItem("editTruckId");
    window.localStorage.setItem("editTruckId", truck.id.toString());
    this.router.navigate(['edit-truck']);
  };

  addTruck(): void {
    this.router.navigate(['add-truck']);
  }

  refresh(): void {
    this.ngOnInit();
  };
}
