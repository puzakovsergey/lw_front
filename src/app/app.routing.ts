import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {AddUserComponent} from "./user/add-user/add-user.component";
import {ListUserComponent} from "./user/list-user/list-user.component";
import {EditUserComponent} from "./user/edit-user/edit-user.component";
import {ListOrderComponent} from "./order/list-order/list-order.component";
import {EditOrderComponent} from "./order/edit-order/edit-order.component";
import {ListDriverComponent} from "./driver/list-driver/list-driver.component";
import {AddDriverComponent} from "./driver/add-driver/add-driver.component";
import {EditDriverComponent} from "./driver/edit-driver/edit-driver.component";
import {ListTruckComponent} from "./truck/list-truck/list-truck.component";
import {AddTruckComponent} from "./truck/add-truck/add-truck.component";
import {EditTruckComponent} from "./truck/edit-truck/edit-truck.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'add-user', component: AddUserComponent },
  { path: 'list-user', component: ListUserComponent },
  { path: 'edit-user', component: EditUserComponent },
  { path: 'list-order', component: ListOrderComponent },
  { path: 'edit-order', component: EditOrderComponent },
  { path: 'list-driver', component: ListDriverComponent },
  { path: 'add-driver', component: AddDriverComponent },
  { path: 'edit-driver', component: EditDriverComponent },
  { path: 'list-truck', component: ListTruckComponent },
  { path: 'add-truck', component: AddTruckComponent },
  { path: 'edit-truck', component: EditTruckComponent },
  {path : '', component : LoginComponent}
];

export const routing = RouterModule.forRoot(routes);
